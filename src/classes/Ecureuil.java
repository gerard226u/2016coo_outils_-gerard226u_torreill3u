package classes;
import java.util.*;
import java.lang.*;

public class Ecureuil implements Animal {

	private int noisette;
	private int pointDeVie;
	
	public Ecureuil(){
		this.noisette = 0;
		this.pointDeVie = 5;
	}
	
	public Ecureuil(int vie){
		this.noisette = 0;
		this.pointDeVie = vie;
	}
	
	@Override
	public boolean etreMort() {
		boolean vivant = false;
		if (this.getPv()>0){
			vivant=true;
		}
		return vivant;
	}

	@Override
	public boolean passerUnjour() {
		boolean journeeOk=false;
		
		if(this.getStockNourriture() > 1){	
			this.noisette -= 1;
			if (getStockNourriture()>0){
				this.noisette-= 1;
				journeeOk = true;
			}
		}else if (this.getStockNourriture() == 0){
			if (this.getPv() > 0){
				this.pointDeVie -= 1;
				journeeOk = true;
			}
		}
		return journeeOk;
	}

	@Override
	public void stockerNourriture(int nourriture) {
		this.noisette += nourriture;

	}

	@Override
	public int getPv() {
		return this.pointDeVie;
	}

	@Override
	public int getStockNourriture() {
		return this.noisette;
	}
	
	public String toString(){
		String res = ("Ecureuil - pv:" + this.getPv() + "noisettes :" + getStockNourriture());
		return res;
	}

}
