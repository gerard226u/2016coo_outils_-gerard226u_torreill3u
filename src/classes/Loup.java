package classes;



public class Loup implements Animal {
	
	int viande;
	int pointDeVie;
	
	public Loup(){
		this.pointDeVie=30;
		this.viande=0;
	}
	
	public Loup(int pdv){
		if (pdv < 0){
			pdv=0;
		}
		this.pointDeVie=pdv;
		this.viande=0;
	}
	
	@Override
	public boolean etreMort(){
		if(this.pointDeVie==0){
			return true;
		}
			return false;
	}

	@Override
	public boolean passerUnjour() {
		boolean res=true;
		if (this.viande==0){
			this.pointDeVie-=4;
			if (this.pointDeVie < 0){
			 this.pointDeVie=0;
			}
		}
		else
		{
			this.viande-=1;
			if (this.viande < 0){
				 this.viande=0;
				}
		}
		this.viande=(this.viande/2);
		if (this.etreMort()==true){
			res = false;
		}
		return res;
	}

	@Override
	public void stockerNourriture(int nourriture) {
		if (nourriture < 0){
			nourriture=0;
		}
		this.viande+=nourriture;
	}

	@Override
	public int getPv() {
		return this.pointDeVie;
	}

	@Override
	public int getStockNourriture() {
		return this.viande;
	}
	
	public String toString(){
		return ("Loup - pv : "+this.getPv()+" viande : "+this.getStockNourriture());
	}

}
