package Test;
import static org.junit.Assert.*;

import org.junit.Test;

import classes.Loup;


public class testLoup {

	@Test
	public void testConstructeur(){
		Loup l1 = new Loup();
		int res = l1.getStockNourriture();
		int pv = l1.getPv();
		assertEquals("nourriture devrait etre a 0", res, 0);
		assertEquals("pdv devrait etre a 30", pv, 30);
	}
	
	@Test
	public void testEtreMort(){
		Loup l1 = new Loup();
		for(int i = 0; i<6; i++){
			l1.passerUnjour();
		}
		boolean ok = l1.etreMort();
		assertEquals("loup devrait etre mort", ok, false);
			
	}
	
	@Test
	public void testPasserUnJour(){
		Loup l1 = new Loup();
		l1.passerUnjour();
		l1.passerUnjour();
		int pv = l1.getPv();
		int viande = l1.getStockNourriture();
		assertEquals("pv devrait etre 22", pv, 22);
		assertEquals("nourriture devrait etre � 0", viande, 0);
	}
		
	@Test
	public void testStockerNourriture(){
		Loup l1 = new Loup();
		l1.stockerNourriture(2);
		int res = l1.getStockNourriture();
		assertEquals("stock devrait etre � 2", res, 2);
	}
	
	@Test
	public void testGetPv(){
		Loup l1 = new Loup(5);
		l1.passerUnjour();
		int res = l1.getPv();
		assertEquals("pdv devrait etre a 1", res,1);
	}
	
	@Test 
	public void testGetStockNourriture(){
		Loup l1 = new Loup();
		l1.stockerNourriture(3);
		int res = l1.getStockNourriture();
		assertEquals("stock devrait etre � 3", res, 3);
	}
	
}
