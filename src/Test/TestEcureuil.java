package Test;
import static org.junit.Assert.*;

import org.junit.Test;

import classes.Ecureuil;


public class TestEcureuil {
	
	@Test
	public void testConstructeur(){
		Ecureuil e1 = new Ecureuil();
		int res = e1.getStockNourriture();
		int pv = e1.getPv();
		assertEquals("nourriture devrait etre a 0", res, 0);
		assertEquals("pdv devrait etre a 5", pv, 5);
	}
	
	@Test
	public void testEtreMort(){
		Ecureuil e1 = new Ecureuil();
		for(int i = 0; i<6; i++){
			e1.passerUnjour();
		}
		boolean ok = e1.etreMort();
		assertEquals("ecureuil devrait etre mort", ok, false);
			
	}
	
	@Test
	public void testPasserUnJour(){
		Ecureuil e1 = new Ecureuil();
		e1.passerUnjour();
		e1.passerUnjour();
		int pv = e1.getPv();
		int noisette = e1.getStockNourriture();
		assertEquals("pv devrait etre 3", pv, 3);
		assertEquals("nourriture devrait etre � 0", noisette, 0);
	}
		
	@Test
	public void testStockerNourriture(){
		Ecureuil e1 = new Ecureuil();
		e1.stockerNourriture(2);
		int res = e1.getStockNourriture();
		assertEquals("stock devrait etre � 2", res, 2);
	}
	
	@Test
	public void testGetPv(){
		Ecureuil e1 = new Ecureuil(5);
		e1.passerUnjour();
		int res = e1.getPv();
		assertEquals("pdv devrait etre a 4", res,4);
	}
	
	@Test 
	public void testGetStockNourriture(){
		Ecureuil e1 = new Ecureuil();
		e1.stockerNourriture(3);
		int res = e1.getStockNourriture();
		assertEquals("stock devrait etre � 3", res, 3);
	}
	
	
	
}


